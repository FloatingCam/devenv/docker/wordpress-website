# WordPress Website

Quickly spin up a 'copy' of a WordPress site on your local workstation using Docker.   A must have for WordPress developers! Don't tamper with a live system until you are sure of the steps and consequences.

With docker-compose and this simple docker-compose.yml file you will be up and running without effort.

See [wiki](//gitlab.com/FloatingCam/devenv/docker/wordpress-website/wikis)